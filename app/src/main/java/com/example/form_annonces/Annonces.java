package com.example.form_annonces;

public class Annonces {
    String titre;
    String photo;
    int prix;
    String video;
    String contact;
    String description;
    String dateDePublication;
    String categorie;
    boolean etat;

    public Annonces(String titre, String photo, int prix, String video, String contact, String description, String dateDePublication, String categorie, boolean etat) {
        this.titre = titre;
        this.photo = photo;
        this.prix = prix;
        this.video = video;
        this.contact = contact;
        this.description = description;
        this.dateDePublication = dateDePublication;
        this.categorie = categorie;
        this.etat = etat;
    }


}
